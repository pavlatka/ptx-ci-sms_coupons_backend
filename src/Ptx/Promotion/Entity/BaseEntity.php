<?php declare(strict_types=1);

namespace Ptx\Promotion\Entity;

abstract class BaseEntity
{
    protected $id;

    public function setId(string $id) : BaseEntity
    {
        $this->id = $id;

        return $this;
    }

    public function getId() : ?string
    {
        return $this->id;
    }
}
