<?php declare(strict_types=1);

namespace Ptx\Promotion\Entity;

class Coupon extends BaseEntity
{
    private $code;
    private $phoneNumber;

    public function setPhoneNumber(string $phoneNumber) : Coupon
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getPhoneNumber() : ?string
    {
        return $this->phoneNumber;
    }

    public function setCode(string $code) : Coupon
    {
        $this->code = $code;

        return $this;
    }

    public function getCode() : ?string
    {
        return $this->code;
    }
}
