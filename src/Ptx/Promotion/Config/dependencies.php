<?php

$container['promotion_coupon_dao'] = function ($c) {
    return new \Ptx\Promotion\Dao\CouponDao(
        $c['mongodb_client'],
        $c['service_coupon_code_generator']
    );
};

$container['promotion_coupon_factory'] = function ($c) {
    return new \Ptx\Promotion\Factory\CouponFactory(
        $c['service_string_modifier']
    );
};

$container['promotion_coupon_repository'] = function ($c) {
    return new \Ptx\Promotion\Repository\CouponRepository(
        $c['promotion_coupon_dao'],
        $c['promotion_coupon_factory']
    );
};

$container['promotion_uc_code_generator'] = function ($c) {
    return new \Ptx\Promotion\UseCase\GenerateCode4Phone\GenerateCode4PhoneUseCase(
        $c['promotion_coupon_repository']
    );
};
