<?php declare(strict_types=1);

namespace Ptx\Promotion\UseCase\GenerateCode4Phone;

class PhoneValidator
{
    public function isValidPhoneNumber(string $phoneNumber) : bool
    {
        $phoneNumber = trim($phoneNumber);

        // TODO: Implement logix
        return !empty($phoneNumber);
    }
}
