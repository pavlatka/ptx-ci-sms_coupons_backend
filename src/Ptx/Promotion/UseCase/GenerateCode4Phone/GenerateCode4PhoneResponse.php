<?php declare(strict_types=1);

namespace Ptx\Promotion\UseCase\GenerateCode4Phone;

use Ptx\Promotion\Entity\Coupon;

class GenerateCode4PhoneResponse
{
    private $coupon;

    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    public function getId() : int
    {
        return $this->coupon->getId();
    }

    public function getCode() : string
    {
        return $this->coupon->getCode();
    }
}
