<?php declare(strict_types=1);

namespace Ptx\Promotion\UseCase\GenerateCode4Phone;

use Ptx\Promotion\UseCase\UseCaseException;

class GenerateCode4PhoneRequest
{
    private $phoneNumber;

    public function __construct(string $phoneNumber)
    {
        $validator = new PhoneValidator();
        if ($validator->isValidPhoneNumber($phoneNumber) !== true) {
            throw new UseCaseException(
                'Phone number is not in correct format.',
                UseCaseException::ERROR_BAD_PARAMETER
            );
        }

        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumber() : string
    {
        return $this->phoneNumber;
    }
}
