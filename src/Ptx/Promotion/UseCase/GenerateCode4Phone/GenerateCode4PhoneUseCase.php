<?php declare(strict_types=1);

namespace Ptx\Promotion\UseCase\GenerateCode4Phone;

use Ptx\Promotion\UseCase\UseCaseException;
use Ptx\Promotion\Repository\CouponRepository;

class GenerateCode4PhoneUseCase
{
    private $repository;

    public function __construct(CouponRepository $repository)
    {
        $this->repository = $repository;
    }

    public function generateCoupon4Phone(GenerateCode4PhoneRequest $request) : GenerateCode4PhoneResponse
    {
        try {
            $phoneNumber = $request->getPhoneNumber();
            $coupon      = $this->repository->generateCoupon4Phone($phoneNumber);

            return new GenerateCode4PhoneResponse($coupon);
        } catch (\Exception $e) {
            throw new UseCaseException($e->getMessage(), $e->getCode());
        }
    }
}
