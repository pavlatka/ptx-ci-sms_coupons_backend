<?php declare(strict_types=1);

namespace Ptx\Promotion\UseCase;

use Ptx\Interfaces\ExceptionInterface;

class UseCaseException extends \Exception implements ExceptionInterface
{
}
