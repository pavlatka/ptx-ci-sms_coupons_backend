<?php declare(strict_types=1);

namespace Ptx\Promotion\Dao;

abstract class BaseDao
{
    protected $mongo;

    public function __construct(\MongoDB\Database $mongo)
    {
        $this->mongo = $mongo;
    }
}
