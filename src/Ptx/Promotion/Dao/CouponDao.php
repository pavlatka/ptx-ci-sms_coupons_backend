<?php declare(strict_types=1);

namespace Ptx\Promotion\Dao;

use Ptx\Promotion\Entity\Coupon;
use Ptx\Service\CouponCodeGenerator;

class CouponDao extends BaseDao
{
    protected $mongodb;
    protected $generator;

    public function __construct(
        \MongoDB\Database $mongodb,
        CouponCodeGenerator $generator
    ) {
        $this->mongodb   = $mongodb;
        $this->generator = $generator;
    }

    public function generateCoupon4Phone(string $phoneNumber) : string
    {
        do {
            $couponCode = $this->generator->generateCouponCode();
        } while ($this->couponCodeInUse($couponCode));

        return $couponCode;
    }

    public function persists(Coupon $coupon) : void
    {
        $couponId   = $coupon->getId();
        $collection = $this->mongodb->promotions;

        $couponData = array(
            'phone_number' => $coupon->getPhoneNumber(),
            'code'         => $coupon->getCode()
        );

        if ($couponId !== null) {
            $couponData['_id'] = $couponId;
        }

        $result = $collection->insertOne($couponData);

        if ($couponId === null) {
            $coupon->setId($result->getInsertedId()->__toString());
        }
    }

    protected function couponCodeInUse(string $couponCode) : bool
    {
        $collection = $this->mongodb->promotions;
        $result = $collection->findOne(array(
            'code' => $couponCode
        ));

        return $result !== null;
    }
}
