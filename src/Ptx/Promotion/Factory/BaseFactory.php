<?php declare(strict_types=1);

namespace Ptx\Promotion\Factory;

use Ptx\Service\StringModifier;
use Ptx\Promotion\Entity\BaseEntity;

abstract class BaseFactory
{
    private $stringModifier;

    public function __construct(StringModifier $stringModifier)
    {
        $this->stringModifier = $stringModifier;
    }

    protected function mapData2Entity(BaseEntity $entity, array $data) : void
    {
        array_walk($data, function (&$value, &$key) use ($entity) {
            if ($value !== null) {
                $setFuntion  = 'set' . $this->stringModifier->camelize($key);
                $entity->$setFuntion($value);
            }
        });
    }
}
