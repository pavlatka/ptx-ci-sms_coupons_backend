<?php declare(strict_types=1);

namespace Ptx\Promotion\Factory;

use Ptx\Promotion\Entity\Coupon;

class CouponFactory extends BaseFactory
{
    public function createEntity(array $data) : Coupon
    {
        $data += array(
            'id'           => null,
            'code'         => null,
            'phone_number' => null
        );

        $entity = new Coupon();
        $this->mapData2Entity($entity, $data);

        return $entity;
    }
}
