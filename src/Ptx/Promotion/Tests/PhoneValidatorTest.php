<?php declare(strict_types=1);

namespace Ptx\Promotion\Tests;

use Ptx\Promotion\UseCase\GenerateCode4Phone\PhoneValidator;

class PhoneValidatorTest extends \PHPUnit\Framework\TestCase
{
    private $validator;

    protected function setUp()
    {
        $this->validator = new PhoneValidator();
    }

    protected function tearDown()
    {
        unset($this->validator);
    }

    public function dataTestIsValidPhoneNumberReturnsCorrectResult()
    {
        return array(
            array('+4917637704911', true),
            array('+35799866855', true),
            array('+123456789', true),
            array('', false),
            array(' ', false),
        );
    }

    /**
     * @dataProvider dataTestIsValidPhoneNumberReturnsCorrectResult
     */
    public function testIsValidPhoneNumberReturnsCorrectResult(string $phoneNumber, bool $expected)
    {
        $isValid = $this->validator->isValidPhoneNumber($phoneNumber);

        if ($expected) {
            $this->assertTrue($isValid);
        } else {
            $this->assertFalse($isValid);
        }
    }
}
