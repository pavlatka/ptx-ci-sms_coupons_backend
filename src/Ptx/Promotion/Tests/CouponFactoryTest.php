<?php declare(strict_types=1);

namespace Ptx\Promotion\Tests;

use Ptx\Service\StringModifier;
use Ptx\Promotion\Entity\Coupon;
use Ptx\Promotion\Factory\CouponFactory;

class CouponFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $factory;

    protected function setUp()
    {
        $stringModifier = new StringModifier();
        $this->factory  = new CouponFactory($stringModifier);
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    public function testCreateEntityReturnCorrectInstanceAndDefaultMapping()
    {
        $entity = $this->factory->createEntity(array());

        $isValid = $entity instanceof Coupon;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals(null, $entity->getCode());
        $this->assertEquals(null, $entity->getPhoneNumber());
    }
}
