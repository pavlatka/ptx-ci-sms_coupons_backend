<?php declare(strict_types=1);

namespace Ptx\Promotion\Tests;

use Ptx\Promotion\Entity\Coupon;

class CouponTest extends \PHPUnit\Framework\TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new Coupon();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetId()
    {
        $value = '12345adsf';
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetPhoneNumber()
    {
        $value = '+4917637704911';
        $this->entity->setPhoneNumber($value);

        $this->assertEquals($value, $this->entity->getPhoneNumber());
    }

    public function testSetGetCode()
    {
        $value = 'ABS123';
        $this->entity->setCode($value);

        $this->assertEquals($value, $this->entity->getCode());
    }
}
