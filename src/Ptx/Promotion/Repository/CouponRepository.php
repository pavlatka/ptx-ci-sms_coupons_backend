<?php declare(strict_types=1);

namespace Ptx\Promotion\Repository;

use Ptx\Promotion\Entity\Coupon;
use Ptx\Promotion\Dao\CouponDao;
use Ptx\Promotion\Factory\CouponFactory;

class CouponRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        CouponDao $dao,
        CouponFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function generateCoupon4Phone(string $phoneNumber) : Coupon
    {
        $couponCode = $this->dao->generateCoupon4Phone($phoneNumber);
        $data = array(
            'code'        => $couponCode,
            'phoneNumber' => $phoneNumber
        );

        $coupon = $this->factory->createEntity($data);

        $this->persists($coupon);

        return $coupon;
    }

    public function persists(Coupon $coupon)
    {
        $this->dao->persists($coupon);
    }
}
