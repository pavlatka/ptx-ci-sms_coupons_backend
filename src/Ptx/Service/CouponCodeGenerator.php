<?php declare(strict_types=1);

namespace Ptx\Service;

class CouponCodeGenerator
{
    public function generateCouponCode() : string
    {
        $couponCode       = null;
        $characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        for ($i = 0; $i < 3; $i++) {
            $couponCode .= $characters[rand(0, $charactersLength -1)];
        }

        $couponCode .= mt_rand(100, 999);

        return $couponCode;
    }
}
