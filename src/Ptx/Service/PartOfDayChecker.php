<?php declare(strict_types=1);

namespace Ptx\Service;

class PartOfDayChecker
{
    public function getPartOfDay(int $timestamp) : string
    {
        $hour = date('H', $timestamp);

        if ($hour <= 12) {
            return 'morning';
        } elseif ($hour <= 20) {
            return 'afternoon';
        }

        return 'night';
    }
}
