<?php declare(strict_types=1);

namespace Ptx\Tests;

use Ptx\Service\PartOfDayChecker;

class PartOfDayCheckerTest extends \PHPUnit\Framework\TestCase
{
    private $service;

    protected function setUp()
    {
        $this->service = new PartOfDayChecker();
    }

    protected function tearDown()
    {
        unset($this->service);
    }

    public function dataTestGetPartOfDay()
    {
        return array(
            array(1488837415, 'night'),
            array(1483257600, 'morning'),
            array(1508508000, 'afternoon'),
        );
    }

    /**
     * @dataProvider dataTestGetPartOfDay
     */
    public function testGetPartOfDay(int $timestamp, string $expected)
    {
        $result = $this->service->getPartOfDay($timestamp);

        $this->assertEquals($expected, $result);
    }
}
