<?php declare(strict_types=1);

namespace Ptx\Notification\Service;

use \Twilio\Rest\Client as Twilio;
use Ptx\Notification\Interfaces\SmsGatewayInterface;

class TwilioSmsGateway implements SmsGatewayInterface
{
    private $twilio;
    private $senderNumber;

    public function __construct(
        Twilio $twilio,
        string $senderNumber
    ) {
        $this->twilio       = $twilio;
        $this->senderNumber = $senderNumber;
    }

    public function sendSms(string $phoneNumber, string $message) : string
    {
        $message = $this->twilio->messages->create(
            $phoneNumber,
            array(
                'from' => $this->senderNumber,
                'body' => $message
            )
        );

        return $message->sid;
    }
}
