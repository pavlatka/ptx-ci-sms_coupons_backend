<?php declare(strict_types=1);

namespace Ptx\Notification\Tests;

use Ptx\Notification\UseCase\SendSms\SendSmsRequest;

class SendSmsRequestTest extends \PHPUnit\Framework\TestCase
{
    public function dataTestConstructThrowsExceptionInCaseDataAreInvalid()
    {
        return array(
            array('', 'message'),
            array(' ', 'message'),
            array('text', ''),
            array('text', ' '),
        );
    }

    /**
     * @expectedException \Ptx\Notification\UseCase\UseCaseException
     * @dataProvider dataTestConstructThrowsExceptionInCaseDataAreInvalid
     */
    public function testConstructThrowsExceptionInCaseDataAreInvalid($phoneNumber, $message)
    {
        new SendSmsRequest($phoneNumber, $message);
    }

    public function testConstructMapsDataProperly()
    {
        $phoneNumber = '+4917637704911';
        $message     = 'Sample message from the system';

        $request = new SendSmsRequest($phoneNumber, $message);

        $this->assertEquals($message, $request->getMessage());
        $this->assertEquals($phoneNumber, $request->getPhoneNumber());
    }
}
