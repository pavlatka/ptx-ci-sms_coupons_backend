<?php declare(strict_types=1);

namespace Ptx\Notification\Tests;

use Ptx\Notification\UseCase\SendSms\SendSmsResponse;

class SendSmsResponseTest extends \PHPUnit\Framework\TestCase
{
    public function testConstructMapsDataProperly()
    {
        $messageId = 'SM0ec1dc0791924f1d8a7a7850b4d4cde0';

        $request = new SendSmsResponse($messageId);

        $this->assertEquals($messageId, $request->getMessageId());
    }
}
