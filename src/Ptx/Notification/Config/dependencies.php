<?php

$container['notification_twillio_gateway'] = function ($c) {
    $senderId     = getEnv('TWILIO_SENDER_ID') ?: null;
    $senderSecret = getEnv('TWILIO_SENDER_SECRET') ?: null;
    $senderNumber = getEnv('TWILIO_SENDER_NUMBER') ?: null;

    $twilioClient = new \Twilio\Rest\Client($senderId, $senderSecret);

    return new \Ptx\Notification\Service\TwilioSmsGateway($twilioClient, $senderNumber);
};

$container['notification_uc_send_sms'] = function ($c) {
    return new \Ptx\Notification\UseCase\SendSms\SendSmsUseCase(
        $c['notification_twillio_gateway']
    );
};
