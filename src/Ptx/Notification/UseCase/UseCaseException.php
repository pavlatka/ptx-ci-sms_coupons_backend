<?php declare(strict_types=1);

namespace Ptx\Notification\UseCase;

use Ptx\Interfaces\ExceptionInterface;

class UseCaseException extends \Exception implements ExceptionInterface
{
}
