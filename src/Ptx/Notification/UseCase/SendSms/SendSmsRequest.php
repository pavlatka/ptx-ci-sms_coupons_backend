<?php declare(strict_types=1);

namespace Ptx\Notification\UseCase\SendSms;

use Ptx\Notification\UseCase\UseCaseException;

class SendSmsRequest
{
    private $message;
    private $phoneNumber;

    public function __construct(string $phoneNumber, string $message)
    {
        $phoneNumber    = trim($phoneNumber);
        $phoneValidator = new PhoneValidator();
        if ($phoneValidator->isValidPhoneNumber($phoneNumber) !== true) {
            throw new UseCaseException(
                'Phone number has not valid format.',
                UseCaseException::ERROR_BAD_PARAMETER
            );
        }

        $message = trim($message);
        if (empty($message)) {
            throw new UseCaseException(
                'Message cannot be left blank.',
                UseCaseException::ERROR_BAD_PARAMETER
            );
        }

        $this->message     = $message;
        $this->phoneNumber = $phoneNumber;
    }

    public function getMessage() : string
    {
        return $this->message;
    }

    public function getPhoneNumber() : string
    {
        return $this->phoneNumber;
    }
}
