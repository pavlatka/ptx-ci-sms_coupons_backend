<?php declare(strict_types=1);

namespace Ptx\Notification\UseCase\SendSms;

class PhoneValidator
{
    public function isValidPhoneNumber(string $phoneNumber) : bool
    {
        $phoneNumber = trim($phoneNumber);

        // TODO: Implement logix
        return !empty($phoneNumber);
    }
}
