<?php declare(strict_types=1);

namespace Ptx\Notification\UseCase\SendSms;

class SendSmsResponse
{
    private $messageId;

    public function __construct(string $messageId)
    {
        $this->messageId = $messageId;
    }

    public function getMessageId() : string
    {
        return $this->messageId;
    }
}
