<?php declare(strict_types=1);

namespace Ptx\Notification\UseCase\SendSms;

use Ptx\Notification\UseCase\UseCaseException;
use Ptx\Notification\Interfaces\SmsGatewayInterface;

class SendSmsUseCase
{
    private $smsGateway;

    public function __construct(SmsGatewayInterface $smsGateway)
    {
        $this->smsGateway = $smsGateway;
    }

    public function sendSms(SendSmsRequest $request) : SendSmsResponse
    {
        try {
            $message     = $request->getMessage();
            $phoneNumber = $request->getPhoneNumber();
            $messageId   = $this->smsGateway->sendSms($phoneNumber, $message);

            return new SendSmsResponse($messageId);
        } catch (\Exception $e) {
            throw new UseCaseException(
                'An error occurred while sending SMS. [T:' . $e->getMessage() . ']',
                UseCaseException::ERROR_RUNTIME_ERROR
            );
        }
    }
}
