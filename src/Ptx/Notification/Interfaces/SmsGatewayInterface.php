<?php declare(strict_types=1);

namespace Ptx\Notification\Interfaces;

interface SMSGatewayInterface
{
    public function sendSms(string $phoneNumber, string $message);
}
