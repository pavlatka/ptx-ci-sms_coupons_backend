<?php declare(strict_types=1);

namespace Ptx\Interfaces;

interface ExceptionInterface
{
    const ERROR_BAD_PARAMETER     = 400;
    const ERROR_UNAUTHORIZED      = 401;
    const ERROR_FORBIDDEN         = 403;
    const ERROR_NOT_FOUND         = 404;
    const ERROR_TOO_MANY_REQUESTS = 429;
    const ERROR_RUNTIME_ERROR     = 500;
}
