<?php declare(strict_types=1);

namespace Ptx\Api\Tests;

use Ptx\Api\Service\ParamValidator;

class ParamValidatorTest extends \PHPUnit\Framework\TestCase
{
    private $validator;

    protected function setUp()
    {
        $this->validator = new ParamValidator();
    }

    protected function tearDown()
    {
        unset($this->validator);
    }

    public function dataTestValidateParamReturnsCorrectResult()
    {
        return array(
            array(' ', false),
            array('', false),
            array('abjsj', true),
            array(0, true),
            array(120, true),
            array('120', true),
        );
    }

    /**
     * @dataProvider dataTestValidateParamReturnsCorrectResult
     */
    public function testValidateParamReturnsCorrectResult($value, $expected)
    {
        $result = $this->validator->validateParam($value);

        if ($expected === true) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
}
