<?php declare(strict_types=1);

namespace Ptx\Api\Tests;

use Ptx\Api\Service\MessageBuilder;

class MessageBuilderTest extends \PHPUnit\Framework\TestCase
{
    private $partOfDayChecker;

    protected function setUp()
    {
        $this->partOfDayChecker = $this->getMockBuilder('\Ptx\Service\PartOfDayChecker')
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        unset($this->partOfDayChecker);
    }

    public function dataTestBuildMessage4CouponCodeReturnsCorrectText()
    {
        return array(
            array('UYT987', 'morning', 'Good Morning! Your promotion code is UYT987.'),
            array('ASB987', 'afternoon', 'Hello! Your promotion code is ASB987.'),
            array('UYT123', 'night', 'Hello! Your promotion code is UYT123.'),
        );
    }

    /**
     * @dataProvider dataTestBuildMessage4CouponCodeReturnsCorrectText
     */
    public function testBuildMessage4CouponCodeReturnsCorrectText($couponCode, $partOfDay, $expected)
    {
        $this->partOfDayChecker->method('getPartOfDay')->willReturn($partOfDay);
        $builder = new MessageBuilder($this->partOfDayChecker);
        $result = $builder->getMessage4CouponCode($couponCode, time());

        $this->assertEquals($expected, $result);
    }
}
