<?php declare(strict_types=1);

namespace Ptx\Api\Tests;

namespace Ptx\Api\Middleware\SecurityMiddleware;

class SecurityMiddlewareTest extends \PHPUnit\Framework\TestCase
{
    private $security;

    protected function setUp()
    {
        $this->security = new SecurityMiddleware();
    }

    protected function tearDown()
    {
        unset($this->security);
    }
}
