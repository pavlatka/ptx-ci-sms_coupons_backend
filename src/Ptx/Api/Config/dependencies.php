<?php

$container['PromotionCouponPostController'] = function ($c) {
    return new \Ptx\Api\Controller\PromotionCouponPostController(
        $c['promotion_uc_code_generator'],
        $c['api_service_param_validator'],
        $c['api_service_message_builder'],
        $c['notification_uc_send_sms']
    );
};

$container['api_service_param_validator'] = function ($c) {
    return new \Ptx\Api\Service\ParamValidator();
};

$container['api_service_message_builder'] = function ($c) {
    return new \Ptx\Api\Service\MessageBuilder(
        $c['service_part_of_day_checker']
    );
};
