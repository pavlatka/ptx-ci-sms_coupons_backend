<?php

$securityLayer     = new \Ptx\Api\Middleware\SecurityMiddleware();
$jsonResponseLayer = new \Ptx\Api\Middleware\JsonResponseMiddleware();
$httpHeaders       = new \Ptx\Api\Middleware\HttpHeadersMiddleware();

$app->group('/api', function () {
    $this->post('/sms_promotions', 'PromotionCouponPostController:process');
})->add($securityLayer)->add($jsonResponseLayer)->add($httpHeaders);
