<?php declare(strict_types=1);

namespace Ptx\Api\Middleware;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

class JsonResponseMiddleware
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) : ResponseInterface {

        $response = $next($request, $response);

        $statusCode = $response->getStatusCode();
        if ($statusCode == 500) {
            return $response->withJson(
                array('message'  => 'An internal error has occured'),
                500
            );
        }

        return $response;
    }
}
