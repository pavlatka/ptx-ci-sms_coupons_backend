<?php declare(strict_types=1);

namespace Ptx\Api\Middleware;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

class SecurityMiddleware
{
    private $authorizedToken;

    public function __construct()
    {
        $this->authorizedToken = getenv('API_TOKEN') ?: 'Bearer MTpwcm9tb3Rpb25fY291cG9ucw==';
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) : ResponseInterface {

        if ($this->authorizeCall($request) !== true) {
            return $response->withJson(
                array('message' => 'You are not authorized to process the call'),
                401
            );
        }

        $response = $next($request, $response);

        return $response;
    }

    protected function authorizeCall(ServerRequestInterface $request) : bool
    {
        $authorization = $request->getHeader('Authorization');
        if (count($authorization) === 0) {
            return false;
        }

        $token = trim($authorization[0]);

        return $token == $this->authorizedToken;
    }
}
