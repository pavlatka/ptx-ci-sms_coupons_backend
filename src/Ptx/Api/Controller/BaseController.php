<?php declare(strict_types=1);

namespace Ptx\Api\Controller;

use \Slim\Http\Request;
use \Slim\Http\Response;

abstract class BaseController
{
    protected $args;
    protected $request;
    protected $response;
    protected $responseStatus = 200;

    public function process(Request $request, Response $response, $args) : Response
    {
        $this->args     = $args;
        $this->request  = $request;
        $this->response = $response;

        try {
            $data = $this->processCall();
        } catch (\Exception $e) {
            $data = array(
                'message' => $e->getMessage()
            );
            $this->setResponseStatus($e->getCode());
        }

        return $response->withJson($data, (int)$this->responseStatus);
    }

    public function getDataFromRequest(string $key, ?string $callback = null)
    {
        $requestBody = $this->request->getBody()->__toString();
        $json        = json_decode($requestBody, true);
        if (is_array($json) && array_key_exists($key, $json)) {
            return $json[$key];
        }

        if ($callback !== null) {
            return $this->request->$callback($key);
        }
    }

    protected function setResponseStatus(int $status)
    {
        if ($status < 100 || $status > 599) {
            $status = 500;
        }

        $this->responseStatus = $status;
    }
}
