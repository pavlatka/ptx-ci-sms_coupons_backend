<?php declare(strict_types=1);

namespace Ptx\Api\Controller;

use Ptx\Promotion\Entity\Coupon;
use Ptx\Api\Service\ParamValidator;
use Ptx\Api\Service\MessageBuilder;
use Ptx\Notification\UseCase\SendSms as Sms;
use Ptx\Promotion\UseCase\GenerateCode4Phone as UseCase;

class PromotionCouponPostController extends BaseController
{
    private $useCase;
    private $smsSender;
    private $paramValidator;
    private $messageBuilder;

    public function __construct(
        UseCase\GenerateCode4PhoneUseCase $useCase,
        ParamValidator $paramValidator,
        MessageBuilder $messageBuilder,
        Sms\SendSmsUseCase $smsSender
    ) {
        $this->useCase        = $useCase;
        $this->paramValidator = $paramValidator;
        $this->messageBuilder = $messageBuilder;
        $this->smsSender      = $smsSender;
    }

    public function processCall() : array
    {
        $phoneNumber = $this->getDataFromRequest('phone_number', 'getParam');
        if ($this->paramValidator->validateParam($phoneNumber) === false) {
            throw new ApiControllerException(
                'Phone_number parameter must be present',
                ApiControllerException::ERROR_BAD_PARAMETER
            );
        }

        $request  = new UseCase\GenerateCode4PhoneRequest($phoneNumber);
        $response = $this->useCase->generateCoupon4Phone($request);

        $couponCode = $response->getCode();
        $this->sendSmsNotification($phoneNumber, $couponCode);

        $this->setResponseStatus(201);

        return array(
            'code' => $couponCode
        );
    }

    protected function sendSmsNotification(string $phoneNumber, string $couponCode)
    {
        $message = $this->messageBuilder->getMessage4CouponCode($couponCode, time());
        $request = new Sms\SendSmsRequest($phoneNumber, $message);

        $this->smsSender->sendSms($request);
    }
}
