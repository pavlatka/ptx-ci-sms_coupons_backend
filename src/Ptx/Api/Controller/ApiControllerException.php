<?php declare(strict_types=1);

namespace Ptx\Api\Controller;

use Ptx\Interfaces\ExceptionInterface;

class ApiControllerException extends \Exception implements ExceptionInterface
{
}
