<?php
namespace Ptx\Api\Service;

class ParamValidator
{
    public function validateParam($value)
    {
        $value = trim($value);

        return !empty($value) || is_numeric($value);
    }
}
