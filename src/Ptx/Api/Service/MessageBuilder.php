<?php declare(strict_types=1);

namespace Ptx\Api\Service;

use Ptx\Service\PartOfDayChecker;

class MessageBuilder
{
    public function __construct(PartOfDayChecker $partOfDayChecker)
    {
        $this->partOfDayChecker = $partOfDayChecker;
    }

    public function getMessage4CouponCode(string $couponCode, int $timestamp) : string
    {
        $partOfDay       = $this->partOfDayChecker->getPartOfDay($timestamp);
        $greetings       = $partOfDay == 'morning' ? 'Good Morning!' : 'Hello!';
        $messageTemplate = ':greetings Your promotion code is :promotion_code.';

        $message = strtr($messageTemplate, array(
            ':greetings'      => $greetings,
            ':promotion_code' => $couponCode
        ));

        return $message;
    }
}
