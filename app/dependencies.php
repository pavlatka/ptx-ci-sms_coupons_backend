<?php

$container = $app->getContainer();

$container['mongodb_client'] = function ($c) {
    $settings = array(
        'host'     => getenv('DB_HOST') ?: 'localhost',
        'port'     => getenv('DB_PORT') ?: 27017,
        'user'     => getenv('DB_USERNAME') ?: null,
        'password' => getenv('DB_PASSWORD') ?: null,
        'database' => getenv('DB_DATABASE') ?: null,
    );

    if ($settings['user'] !== null && $settings['password'] !== null) {
        $connector = sprintf(
            'mongodb://%s:%s@%s:%d/%s',
            $settings['user'],
            $settings['password'],
            $settings['host'],
            $settings['port'],
            $settings['database']
        );
    } else {
        $connector = sprintf(
            'mongodb://%s:%d/%s',
            $settings['host'],
            $settings['port'],
            $settings['database']
        );
    }

    $mongoClient = new \MongoDB\Client($connector);
    $database = $mongoClient->selectDatabase($settings['database']);

    return $database;
};

$container['service_string_modifier'] = function ($c) {
    return new \Ptx\Service\StringModifier();
};

$container['service_part_of_day_checker'] = function ($c) {
    return new \Ptx\Service\PartOfDayChecker();
};

$container['service_coupon_code_generator'] = function ($c) {
    return new \Ptx\Service\CouponCodeGenerator();
};

require_once __DIR__ . '/../src/Ptx/Api/Config/dependencies.php';
require_once __DIR__ . '/../src/Ptx/Notification/Config/dependencies.php';
require_once __DIR__ . '/../src/Ptx/Promotion/Config/dependencies.php';
