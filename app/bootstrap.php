<?php

require_once __DIR__ . '/../vendor/autoload.php';
$errorReporting = getEnv('ERROR_REPORTING') ?: 0;
$config = array(
    'settings' => array(
        'displayErrorDetails' => true
    )
);
$app = new \Slim\App($config);

require_once __DIR__ . '/dependencies.php';
require_once __DIR__ . '/routes.php';

$app->run();
