# README #

The API application to provide support for promotions. In current version the system just support one end point. 

### Swagger Documentation? ###

* [Api Documentation](https://app.swaggerhub.com/api/pavlatka/SMSPromotions/1.0.0)

### Where to se it live? ###

* [Live version](https://ptxcoupons.herokuapp.com) - *it's a free heroku dyno, so it might take some to start* 

### Possible settings? ###

I do not like to hard-code anything external, so the system uses environment variables for such cases. For the simplicity, you can change it under `/public/.htaccess` file, but I would prefer to set it up e.g. in heroku administration.

### Known issues ###

**1. MongoDb**

The system on live is connected to a free MongoDB database (where it collects data about promotion codes). The truth is that I have encountered multiple times, that the connection was not stable and it failed. In that case, the system return HTTP Code 500, but this should not be a case in production environment. 

**2. Twilio.com**

The problem is that in test version you can send messages only to the test mobile device. If you try to send to another device Twilio.com will not allow that. So you might need to create your own account to test this. But if you do so, you have to update some data in the `/public/.htaccess` file.

**3. Security**

Right now all the access tokens are visible in `/public/.htaccess` files which is not right. I was struggling with this idea, but for the time being it's ok. I will reset all the passwords once I know that you have tested it. On top this repository is not meant to be public, but it was made public so you might have an access to.

### What I would update if I had more time ###

I do not like the fact, that the API call communicates directly with Twilio.com. That's not sufficient and should be replace with better logic. The API user doesn't need to wait until the communication with the Twilio.com is done. 

The idea would be to have a messaging app (RabbitMQ or similar) and the API would just send information there that there is something to be done. There would be another application, which would be "listening" to this and process certain action in case a new SMS (in our case) should be sent. 

But as you can imagine, this would take more time to implement and I did not have it - which is sad :(.